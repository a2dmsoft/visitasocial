package br.com.integracao.visitasocial.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.integracao.visitasocial.dao.impl.PacienteDaoMVImpl;
import br.com.integracao.visitasocial.dao.impl.RelatorioDaoImpl;
import br.com.integracao.visitasocial.dao.impl.VisitaSocialDaoImpl;
import br.com.integracao.visitasocial.model.Paciente;
import br.com.integracao.visitasocial.response.VisitaSocialResponse;
import br.com.integracao.visitasocial.utils.IntegracaoUtils;

public class IntegracaoService
{
	static Logger logger = Logger.getLogger(IntegracaoService.class);
	
	public List<VisitaSocialResponse> processar() throws Exception
	{
		List<VisitaSocialResponse> listVisitaSocialResponse = new ArrayList<>();
		List<Paciente> listPaciente = new ArrayList<>();
		
		listPaciente.addAll(new PacienteDaoMVImpl().obterDados());
		listPaciente.addAll(new RelatorioDaoImpl().processarPacientes());
		
		if (listPaciente != null
				&& listPaciente.size() > 0)
		{
			for (Paciente paciente : listPaciente)
			{
				VisitaSocialResponse response = new VisitaSocialDaoImpl().inserir(paciente);
				addLoggerInfo(response);
				listVisitaSocialResponse.add(response);
			}
		}
		return listVisitaSocialResponse;
	}
	
	private void addLoggerInfo(VisitaSocialResponse response)
	{
		if (response.getSucesso()
				&& response.getRetorno() != null)
		{
			String acao = "inserida";
			
			if (IntegracaoUtils.isPacientePrioridade(response.getRetorno().getPrioridade()))
			{
				acao = "alterada";
			}
			
			logger.info("Visita Social " + acao + " com sucesso. ID: " + response.getRetorno().getIdVisitaSocial());
		}
		else
		{
			logger.info(response.getMensagem());
		}
	}
}