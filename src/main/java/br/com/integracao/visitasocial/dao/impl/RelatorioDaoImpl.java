package br.com.integracao.visitasocial.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.integracao.visitasocial.dao.RelatorioDao;
import br.com.integracao.visitasocial.factory.ConnectionFactory;
import br.com.integracao.visitasocial.model.GeradorProcessamento;
import br.com.integracao.visitasocial.model.Paciente;
import br.com.integracao.visitasocial.utils.ConnectionUtils;
import br.com.integracao.visitasocial.utils.DateUtils;

public class RelatorioDaoImpl implements RelatorioDao
{
	static Logger logger = Logger.getLogger(RelatorioDaoImpl.class);
	
	@Override
	public List<Paciente> processarPacientes() throws SQLException
	{
		logger.info("Obtendo dados Sabius");
		ConnectionFactory instanceConn = ConnectionUtils.configConnSabius();
		List<Paciente> listPaciente = new ArrayList<>();
		
		try (Connection conn = instanceConn.createConnection())
		{
			listPaciente.addAll(autorizacoesSolicitacoesCedidas(conn));
//			listPaciente.addAll(correcaoGeracaoDiasUteis(conn));
			listPaciente.addAll(associacaoCearenseMagistrado(conn));
			listPaciente.addAll(associacaoCearenseMinisterioPublico(conn));
			listPaciente.addAll(coelceExecutivo(conn));
			listPaciente.addAll(contratosMultimedicos(conn));
		} 
		catch (SQLException ex)
		{
			logger.error(ex.getMessage());
			throw new SQLException(ex.getMessage());
		}
		logger.info("Dados do Sabius obtidos com sucesso");
		return listPaciente;
	}
	
	private List<Paciente> contratosMultimedicos(Connection conn) throws SQLException 
	{
		List<Paciente> listPaciente = new ArrayList<>();
		
		try (PreparedStatement pst = conn.prepareStatement(buildSqlSelectFromContratosMultimedicos());
			 ResultSet rs = pst.executeQuery();)
		{
			while (rs.next())
			{
				listPaciente.add(resultSetToObjectFromContratosMultimedicos(rs));
			}
			return listPaciente;
		}
		catch (SQLException ex)
		{
			logger.error(ex.getMessage());
			throw new SQLException(ex.getMessage());
		}
	}

	private Paciente resultSetToObjectFromContratosMultimedicos(ResultSet rs) throws SQLException 
	{
		int i = 1;
		Paciente paciente = new Paciente();
		
		paciente.setNrCarteira(rs.getString(i++));
		paciente.setNmPaciente(rs.getString(i++));
		paciente.setDtNascimento(rs.getDate(i++));
		paciente.setDtAtendimento(rs.getString(i++));
		paciente.setCdUf(rs.getString(i++));
		paciente.setNrFone(rs.getString(i++));
		paciente.setPrioridade(new Integer(1));
		
		return paciente;
	}
	
	private String buildSqlSelectFromContratosMultimedicos() 
	{
		String sql = " SELECT " + 
				"PPP.CARTEIRA, " + 
				"PPP.NOME_BENEFICIARIO, " + 
				"PPP.DATA_NASCIMENTO, " + 
				"PPP.DATA_INTERNACAO, " + 
				"PPP.CRM, " + 
				"NVL((select distinct RPAD(nvl(mc2.numero_contato,' '), 11, ' ') " + 
				"from enderecos_prestador ep2, meio_contato mc2 " + 
				"where ep2.cod_prestador(+) = PPP.CRM " + 
				"and mc2.cod_endereco(+) = ep2.cod_endereco " + 
				"and mc2.cod_tipo_meio_contato = 5 " + 
				"AND ROWNUM < 2),'--') CELULAR_TITULAR " + 
				"FROM " + 
				"( " + 
				"select LPAD(G.COD_UNIMED, '3', '0') || LPAD(G.COD_CARTEIRA || G.DV_CARTEIRA,'13','0') CARTEIRA, " + 
				"g.COD_CONTRATO, " + 
				"B.NOME_BENEFICIARIO, " + 
				"CASE WHEN CB.cod_dependencia='T' THEN 'Titular' ELSE 'Dependente' END DEPENDENCIA, " + 
				"B.DATA_NASCIMENTO, " + 
				"CASE WHEN CB.cod_dependencia='D' THEN " + 
				"(SELECT b2.nome_beneficiario " + 
				"FROM beneficiario b2, contratos_beneficiario cb1 " + 
				"WHERE b2.cod_beneficiario = cb1.cod_beneficiario " + 
				"and CB.cod_unimed = cb1.cod_unimed " + 
				"and CB.cod_empresa = cb1.cod_empresa " + 
				"and CB.cod_familia = cb1.cod_familia " + 
				"and CB.cod_contrato = cb1.cod_contrato " + 
				"AND cb1.cod_dependencia = 'T' " + 
				"AND ROWNUM < 2) " + 
				"ELSE '--' " + 
				"end NOME_TITULAR, " + 
				"CASE WHEN CB.cod_dependencia='D' THEN " + 
				"(SELECT ba.DESC_PARENTESCO " + 
				"FROM parentesco ba " + 
				"WHERE 1=1 " + 
				"AND CB.COD_PARENTESCO = BA.COD_PARENTESCO " + 
				"AND ROWNUM < 2) " + 
				"ELSE '--' " + 
				"end GRAU_PARENTESCO, " + 
				"CASE WHEN CB.cod_dependencia='D' THEN " + 
				"(SELECT CP.COD_PRESTADOR " + 
				"FROM beneficiario b2, contratos_beneficiario cb1, COMPL_PRESTADOR_PF CP, PRESTADOR P2 " + 
				"WHERE b2.cod_beneficiario = cb1.cod_beneficiario " + 
				"and CB.cod_unimed = cb1.cod_unimed " + 
				"and CB.cod_empresa = cb1.cod_empresa " + 
				"and CB.cod_familia = cb1.cod_familia " + 
				"and CB.cod_contrato = cb1.cod_contrato " + 
				"AND cb1.cod_dependencia = 'T' " + 
				"AND CP.CPF = B2.CPF " + 
				"AND P2.COD_PRESTADOR = CP.COD_PRESTADOR " + 
				"AND P2.UNIMED_PRESTADOR = 63 " + 
				"AND ROWNUM < 2 " + 
				") " + 
				"ELSE " + 
				"(SELECT CP.COD_PRESTADOR " + 
				"FROM COMPL_PRESTADOR_PF CP, PRESTADOR P2 " + 
				"WHERE CP.CPF = B.CPF " + 
				"AND P2.COD_PRESTADOR = CP.COD_PRESTADOR " + 
				"AND P2.UNIMED_PRESTADOR = 63 " + 
				"AND ROWNUM < 2 " + 
				") " + 
				"end CRM, " + 
				"g.COD_HOSPITAL, P.NOME_PRESTADOR, " + 
				"G.NUM_NOTA, " + 
				"G.DATA_INTERNACAO, " + 
				"G.OBSERVACOES_GUIA, " + 
				"LINHAS_PARA_COLUNA('select ''( Cod.: '' || SHM.serv_med_hosp || SHM.digito_servico || '' # Desc.: '' || SA.descricao || '' # Qtd: '' || SHM.qtde_utilizada || '' )'' " + 
				"from servicos_guias_hosp SHM, SERV_MEDICOS_HOSPITALARES SA where SHM.num_nota = ' || g.num_nota || ' " + 
				"AND SA.COD_SERVICO = SHM.SERV_MED_HOSP order by 1') Servicos, " + 
				"case when g.tipo_prorrogacao = 1 " + 
				"then 'Não' " + 
				"else " + 
				"'Sim' end as alta_administrativa " + 
				"from guias_hosp g, beneficiario b, PRESTADOR P, Contratos_Beneficiario CB " + 
				"where g.situacao_atual not in (0,3) " + 
				"and g.data_alta is NULL " + 
				"and g.tipo_prorrogacao IN (1,3) " + 
				"and b.cod_beneficiario = g.cod_beneficiario " + 
				"and g.cod_contrato IN (4700,7003,7004) " + 
				"AND P.COD_PRESTADOR = G.COD_HOSPITAL " + 
				"AND CB.COD_BENEFICIARIO = G.COD_BENEFICIARIO " + 
				"AND CB.COD_UNIMED = G.COD_UNIMED " + 
				"AND CB.COD_EMPRESA = G.COD_EMPRESA " + 
				"AND CB.COD_FAMILIA = G.COD_FAMILIA " + 
				"AND CB.COD_CONTRATO = G.COD_CONTRATO " + 
				") PPP " + 
				"where 1 = 1 " + 
				"and COD_HOSPITAL = 11005116 " + 
				"and to_date(DATA_INTERNACAO,'DD/MM/YYYY') between to_date(sysdate-2,'DD/MM/YYYY') and to_date(sysdate,'DD/MM/YYYY')";
		
		return sql;
	}
	
	private List<Paciente> coelceExecutivo(Connection conn) throws SQLException 
	{
		List<Paciente> listPaciente = new ArrayList<>();
		
		try (PreparedStatement pst = conn.prepareStatement(buildSqlSelectFromCoelceExecutivo());
			 ResultSet rs = pst.executeQuery();)
		{
			while (rs.next())
			{
				listPaciente.add(resultSetToObjectFromCoelceExecutivo(rs));
			}
			return listPaciente;
		}
		catch (SQLException ex)
		{
			logger.error(ex.getMessage());
			throw new SQLException(ex.getMessage());
		}
	}
	
	private Paciente resultSetToObjectFromCoelceExecutivo(ResultSet rs) throws SQLException 
	{
		int i = 1;
		Paciente paciente = new Paciente();
		
		paciente.setNrCarteira(rs.getString(i++));
		paciente.setNmPaciente(rs.getString(i++));
		paciente.setDtNascimento(rs.getDate(i++));
		paciente.setDtAtendimento(rs.getString(i++));
		paciente.setCdUf(rs.getString(i++));
		paciente.setDsConPlan(rs.getString(i++));
		paciente.setDsCodigoConselho(new Integer(rs.getString(i++)));
		paciente.setNrFone(rs.getString(i++));
		paciente.setPrioridade(new Integer(1));
		
		return paciente;
	}

	private String buildSqlSelectFromCoelceExecutivo() 
	{
		String sql = " SELECT " +
				" PPP.CARTEIRA, " +
				" PPP.NOME_BENEFICIARIO, " +
				" PPP.DATA_NASCIMENTO, " +
				" PPP.DATA_INTERNACAO, " +
				" PPP.CRM, " +
			    " PPP.desc_plano, " +
			    " PPP.conselho_profissional, " +
				" NVL((select distinct RPAD(nvl(mc2.numero_contato,' '), 11, ' ') " +
				" from enderecos_prestador ep2, meio_contato mc2 " +
				" where ep2.cod_prestador(+) = PPP.CRM " +
				" and mc2.cod_endereco(+) = ep2.cod_endereco " +
				" and mc2.cod_tipo_meio_contato = 5 " +
				" AND ROWNUM < 2),'--') CELULAR_TITULAR, " +
				" PPP.ALTA_ADMINISTRATIVA " +
				" FROM " +
				" ( " +
				" select LPAD(G.COD_UNIMED, '3', '0') || LPAD(G.COD_CARTEIRA || G.DV_CARTEIRA,'13','0') CARTEIRA, " +
				" g.COD_CONTRATO, " +
				" B.NOME_BENEFICIARIO, " +
				" CASE WHEN CB.cod_dependencia='T' THEN 'Titular' ELSE 'Dependente' END DEPENDENCIA, " +
				" B.DATA_NASCIMENTO, " +
				" CASE WHEN CB.cod_dependencia='D' THEN " +
				" (SELECT b2.nome_beneficiario " +
				" FROM beneficiario b2, contratos_beneficiario cb1 " +
				" WHERE b2.cod_beneficiario = cb1.cod_beneficiario " +
				" and CB.cod_unimed = cb1.cod_unimed " +
				" and CB.cod_empresa = cb1.cod_empresa " +
				" and CB.cod_familia = cb1.cod_familia " +
				" and CB.cod_contrato = cb1.cod_contrato " +
				" AND cb1.cod_dependencia = 'T' " +
				" AND ROWNUM < 2) " +
				" ELSE '--' " +
				" end NOME_TITULAR, " +
				" CASE WHEN CB.cod_dependencia='D' THEN " +
				" (SELECT ba.DESC_PARENTESCO " +
				" FROM parentesco ba " +
				" WHERE 1=1 " +
				" AND CB.COD_PARENTESCO = BA.COD_PARENTESCO " +
				" AND ROWNUM < 2) " +
				" ELSE '--' " +
				" end GRAU_PARENTESCO, " +
				" CASE WHEN CB.cod_dependencia='D' THEN " +
				" (SELECT CP.COD_PRESTADOR " +
				" FROM beneficiario b2, contratos_beneficiario cb1, COMPL_PRESTADOR_PF CP, PRESTADOR P2 " +
				" WHERE b2.cod_beneficiario = cb1.cod_beneficiario " +
				" and CB.cod_unimed = cb1.cod_unimed " +
				" and CB.cod_empresa = cb1.cod_empresa " +
				" and CB.cod_familia = cb1.cod_familia " +
				" and CB.cod_contrato = cb1.cod_contrato " +
				" AND cb1.cod_dependencia = 'T' " +
				" AND CP.CPF = B2.CPF " +
				" AND P2.COD_PRESTADOR = CP.COD_PRESTADOR " +
				" AND P2.UNIMED_PRESTADOR = 63 " +
				" AND ROWNUM < 2 " +
				" ) " +
				" ELSE " +
				" (SELECT CP.COD_PRESTADOR " +
				" FROM COMPL_PRESTADOR_PF CP, PRESTADOR P2 " +
				" WHERE CP.CPF = B.CPF " +
				" AND P2.COD_PRESTADOR = CP.COD_PRESTADOR " +
				" AND P2.UNIMED_PRESTADOR = 63 " +
				" AND ROWNUM < 2 " +
				") " +
				" end CRM, " +
				" g.COD_HOSPITAL, P.NOME_PRESTADOR, " +
				" G.NUM_NOTA, " +
				" G.DATA_INTERNACAO, " +
				" pc.desc_plano, " +
	            " cp.conselho_profissional," +
				" G.OBSERVACOES_GUIA, " +
				" LINHAS_PARA_COLUNA('select ''( Cod.: '' || SHM.serv_med_hosp || SHM.digito_servico || '' # Desc.: '' || SA.descricao || '' # Qtd: '' || SHM.qtde_utilizada || '' )'' " +
				" from servicos_guias_hosp SHM, SERV_MEDICOS_HOSPITALARES SA where SHM.num_nota = ' || g.num_nota || ' " +
				" AND SA.COD_SERVICO = SHM.SERV_MED_HOSP order by 1') Servicos, " +
				" case when g.tipo_prorrogacao = 1 " +
				" then 'Não' " +
				" else " +
				" 'Sim' end as alta_administrativa " +
				" from guias_hosp g, beneficiario b, PRESTADOR P, Contratos_Beneficiario CB, planos_contrato pc, compl_prestador_pf cp " +
				" where g.situacao_atual not in (0,3) " +
				" and g.data_alta is NULL " +
				" and g.tipo_prorrogacao IN (1,3) " +
				" AND g.cod_hospital = 11005116 " +
				" and b.cod_beneficiario = g.cod_beneficiario " +
				" AND CB.cod_contrato = pc.cod_contrato " +
		        " AND P.cod_prestador = cp.cod_prestador " +
				" and g.cod_contrato IN (4700,7003,7004) " +
				" AND P.COD_PRESTADOR = G.COD_HOSPITAL " +
				" AND CB.COD_BENEFICIARIO = G.COD_BENEFICIARIO " +
				" AND CB.COD_UNIMED = G.COD_UNIMED " +
				" AND CB.COD_EMPRESA = G.COD_EMPRESA " +
				" AND CB.COD_FAMILIA = G.COD_FAMILIA " +
				" AND CB.COD_CONTRATO = G.COD_CONTRATO " +
				" ) PPP " +
				" where 1 = 1 " +
				" and COD_HOSPITAL = 11005116 " +
				" and to_date(DATA_INTERNACAO,'DD/MM/YYYY') between to_date(sysdate-2,'DD/MM/YYYY') and to_date(sysdate,'DD/MM/YYYY')";
		return sql;
	}

	private List<Paciente> associacaoCearenseMagistrado(Connection conn) throws SQLException 
	{
		List<Paciente> listPaciente = new ArrayList<>();
		
		try (PreparedStatement pst = conn.prepareStatement(buildSqlSelectFromAssociacaoMagistrado());
			 ResultSet rs = pst.executeQuery();)
		{
			while (rs.next())
			{
				listPaciente.add(resultSetToObjectFromAssociacaoMagistrado(rs));
			}
			return listPaciente;
		}
		catch (SQLException ex)
		{
			logger.error(ex.getMessage());
			throw new SQLException(ex.getMessage());
		}
	}
	
	private List<Paciente> associacaoCearenseMinisterioPublico(Connection conn) throws SQLException 
	{
		List<Paciente> listPaciente = new ArrayList<>();
		
		try (PreparedStatement pst = conn.prepareStatement(buildSqlSelectFromAssociacaoCearenseMinisterioPublico());
			 ResultSet rs = pst.executeQuery();)
		{
			while (rs.next())
			{
				listPaciente.add(resultSetToObjectFromAssociacaoCearenseMinisterioPublico(rs));
			}
			return listPaciente;
		}
		catch (SQLException ex)
		{
			logger.error(ex.getMessage());
			throw new SQLException(ex.getMessage());
		}
	}

	private Paciente resultSetToObjectFromAssociacaoCearenseMinisterioPublico(ResultSet rs) throws SQLException 
	{
		int i = 1;
		Paciente paciente = new Paciente();
		
		paciente.setNrCarteira(rs.getString(i++));
		paciente.setNmPaciente(rs.getString(i++));
		paciente.setDtNascimento(rs.getDate(i++));
		paciente.setDtAtendimento(rs.getString(i++));
		paciente.setCdUf(rs.getString(i++));
		paciente.setDsConPlan(rs.getString(i++));
		paciente.setDsCodigoConselho(new Integer(rs.getString(i++)));
		paciente.setNrFone(rs.getString(i++));
		paciente.setPrioridade(new Integer(1));
		
		return paciente;
	}

	private String buildSqlSelectFromAssociacaoCearenseMinisterioPublico() 
	{
		String sql = " SELECT " + 
				" PPP.CARTEIRA, " +
				" PPP.NOME_BENEFICIARIO, " +
				" PPP.DATA_NASCIMENTO, " +
				" PPP.DATA_INTERNACAO, " +
				" PPP.crm, " + 
			    " PPP.desc_plano, " +
			    " PPP.conselho_profissional, " +
				" NVL((select distinct RPAD(nvl(mc2.numero_contato, ' '), 11, ' ') " + 
				"       from ver_enderecos_benef ep2, meio_contato mc2 " + 
				"      where ep2.cod_beneficiario(+) = PPP.COD_BENEF_TITULAR " + 
				"        AND ep2.tipo_endereco = 'R' " + 
				"        and mc2.cod_endereco(+) = ep2.cod_endereco " + 
				"        and mc2.cod_tipo_meio_contato = 5 " + 
				"        AND ROWNUM < 2), " + 
				"     '--') CELULAR_TITULAR, " + 
				" PPP.ALTA_ADMINISTRATIVA " + 
				" FROM " + 
				" ( " + 
				" select LPAD(G.COD_UNIMED, '3', '0') || LPAD(G.COD_CARTEIRA || G.DV_CARTEIRA,'13','0') CARTEIRA, " + 
				" g.COD_CONTRATO, " + 
				" B.NOME_BENEFICIARIO, " + 
				" CASE WHEN CB.cod_dependencia='T' THEN 'Titular' ELSE 'Dependente' END DEPENDENCIA, " + 
				" B.DATA_NASCIMENTO, " + 
				" CASE WHEN CB.cod_dependencia='D' THEN " + 
				" (SELECT b2.nome_beneficiario " + 
				" FROM beneficiario b2, contratos_beneficiario cb1 " + 
				" WHERE b2.cod_beneficiario = cb1.cod_beneficiario " + 
				" and CB.cod_unimed = cb1.cod_unimed " + 
				" and CB.cod_empresa = cb1.cod_empresa " + 
				" and CB.cod_familia = cb1.cod_familia " + 
				" and CB.cod_contrato = cb1.cod_contrato " + 
				" AND cb1.cod_dependencia = 'T' " + 
				" AND ROWNUM < 2) " + 
				" ELSE '--' " + 
				" end NOME_TITULAR, " + 
				" CASE WHEN CB.cod_dependencia='D' THEN " + 
				" (SELECT ba.DESC_PARENTESCO " + 
				" FROM parentesco ba " + 
				" WHERE 1=1 " + 
				" AND CB.COD_PARENTESCO = BA.COD_PARENTESCO " + 
				" AND ROWNUM < 2) " + 
				" ELSE '--' " + 
				" end GRAU_PARENTESCO, " + 
				" CASE " + 
				"  WHEN CB.cod_dependencia = 'D' THEN " + 
				"   (SELECT b2.COD_beneficiario " + 
				"      FROM beneficiario b2, contratos_beneficiario cb1 " + 
				"     WHERE b2.cod_beneficiario = cb1.cod_beneficiario " + 
				"       and CB.cod_unimed = cb1.cod_unimed " + 
				"       and CB.cod_empresa = cb1.cod_empresa " + 
				"       and CB.cod_familia = cb1.cod_familia " + 
				"       and CB.cod_contrato = cb1.cod_contrato " + 
				"       AND cb1.cod_dependencia = 'T' " + 
				"       AND ROWNUM < 2) " + 
				"  ELSE " + 
				"   B.COD_BENEFICIARIO " + 
				" end " + 
				" COD_BENEF_TITULAR, " + 
				" g.COD_HOSPITAL, P.NOME_PRESTADOR, " + 
				" G.NUM_NOTA, " + 
				" G.DATA_INTERNACAO, " +
				" CASE  " +
				" WHEN CB.cod_dependencia = 'D' THEN (SELECT CP.cod_prestador  " +
				"                                     FROM   beneficiario b2, " + 
				" contratos_beneficiario cb1, " + 
				" compl_prestador_pf CP, " + 
				" prestador P2  " +
				"                                     WHERE  " +
				" b2.cod_beneficiario = cb1.cod_beneficiario " + 
				" AND CB.cod_unimed = cb1.cod_unimed  " +
				" AND CB.cod_empresa = cb1.cod_empresa " + 
				" AND CB.cod_familia = cb1.cod_familia  " +
				" AND CB.cod_contrato = cb1.cod_contrato " + 
				" AND cb1.cod_dependencia = 'T' " + 
				" AND CP.cpf = B2.cpf  " +
				" AND P2.cod_prestador = CP.cod_prestador " + 
				" AND P2.unimed_prestador = 63 " + 
				" AND ROWNUM < 2)  " +
				" ELSE (SELECT CP.cod_prestador  " +
				"       FROM   compl_prestador_pf CP, " + 
				"              prestador P2  " +
				"       WHERE  CP.cpf = B.cpf  " +
				"              AND P2.cod_prestador = CP.cod_prestador " + 
				"              AND P2.unimed_prestador = 63 " + 
				"              AND ROWNUM < 2)  " +
				"  END                                                       CRM, " +
				" pc.desc_plano, " +
	            " cp.conselho_profissional," +
				" G.OBSERVACOES_GUIA, " + 
				" LINHAS_PARA_COLUNA('select ''( Cod.: '' || SHM.serv_med_hosp || SHM.digito_servico || '' # Desc.: '' || SA.descricao || '' # Qtd: '' || SHM.qtde_utilizada || '' )'' " + 
				" from servicos_guias_hosp SHM, SERV_MEDICOS_HOSPITALARES SA where SHM.num_nota = ' || g.num_nota || ' " + 
				" AND SA.COD_SERVICO = SHM.SERV_MED_HOSP order by 1') Servicos, " + 
				" case when g.tipo_prorrogacao = 1 " + 
				" then 'Não' " + 
				" else " + 
				" 'Sim' end as alta_administrativa " + 
				" from guias_hosp g, beneficiario b, PRESTADOR P, Contratos_Beneficiario CB, planos_contrato pc, compl_prestador_pf cp " + 
				" where g.situacao_atual not in (0,3) " + 
				" and g.data_alta is NULL " + 
				" and g.tipo_prorrogacao IN (1,3) " + 
				" and b.cod_beneficiario = g.cod_beneficiario " + 
				" and (     (G.COD_EMPRESA in (6208, 6293)) " + 
				"       or (G.COD_EMPRESA = 69 and g.cod_contrato in (3357, 3358, 3391) ) " + 
				"    ) " + 
				" AND P.COD_PRESTADOR = G.COD_HOSPITAL " + 
				" and COD_HOSPITAL = 11005116 " + 
				" AND CB.COD_BENEFICIARIO = G.COD_BENEFICIARIO " + 
				" AND CB.COD_UNIMED = G.COD_UNIMED " + 
				" AND CB.COD_EMPRESA = G.COD_EMPRESA " + 
				" AND CB.COD_FAMILIA = G.COD_FAMILIA " + 
				" AND CB.COD_CONTRATO = G.COD_CONTRATO " + 
				" AND CB.cod_contrato = pc.cod_contrato " +
		        " AND P.cod_prestador = cp.cod_prestador " +
				" and G.COD_UNIMED = 63 " + 
				" and nvl(g.COD_UNIMED, 0) > 0 " + 
				" and nvl(G.COD_EMPRESA, 0) > 0 " + 
				" and CB.COD_UNIMED = 63 " + 
				" and nvl(CB.COD_UNIMED, 0) > 0 " + 
				" and nvl(CB.COD_EMPRESA, 0) > 0 " + 
				" and nvl(CB.COD_FAMILIA, 0) > -1 " + 
				" and nvl(CB.COD_CONTRATO, 0) > -1 " + 
				" and nvl(CB.COD_BENEFICIARIO, 0) > 0 " + 
				" ) PPP " + 
				" where 1 = 1 " + 
				" and to_date(DATA_INTERNACAO,'DD/MM/YYYY') between to_date(sysdate-2,'DD/MM/YYYY') and to_date(sysdate,'DD/MM/YYYY')";
		return sql;
	}

	private Paciente resultSetToObjectFromAssociacaoMagistrado(ResultSet rs) throws SQLException 
	{
		int i = 1;
		Paciente paciente = new Paciente();
		
		paciente.setNrCarteira(rs.getString(i++));
		paciente.setNmPaciente(rs.getString(i++));
		paciente.setDtNascimento(rs.getDate(i++));
		paciente.setDtAtendimento(rs.getString(i++));
		paciente.setCdUf(rs.getString(i++));
		paciente.setDsConPlan(rs.getString(i++));
		paciente.setDsCodigoConselho(new Integer(rs.getString(i++)));
		paciente.setNrFone(rs.getString(i++));
		paciente.setPrioridade(new Integer(1));
		
		return paciente;
	}

	private String buildSqlSelectFromAssociacaoMagistrado() 
	{
		String sql = " SELECT " + 
				" PPP.CARTEIRA, " + 
				" PPP.NOME_BENEFICIARIO, " + 
				" PPP.DATA_NASCIMENTO, " +
				" PPP.DATA_INTERNACAO, " + 
				" PPP.crm, " + 
			    " PPP.desc_plano, " +
			    " PPP.conselho_profissional, " +
				" NVL((select distinct RPAD(nvl(mc2.numero_contato, ' '), 11, ' ') " + 
				"       from ver_enderecos_benef ep2, meio_contato mc2 " + 
				"      where ep2.cod_beneficiario(+) = PPP.COD_BENEF_TITULAR " + 
				"        AND ep2.tipo_endereco = 'R' " + 
				"        and mc2.cod_endereco(+) = ep2.cod_endereco " + 
				"        and mc2.cod_tipo_meio_contato = 5 " + 
				"        AND ROWNUM < 2), " + 
				"     '--') CELULAR_TITULAR, " + 
				" PPP.ALTA_ADMINISTRATIVA " + 
				" FROM " + 
				" ( " + 
				" select LPAD(G.COD_UNIMED, '3', '0') || LPAD(G.COD_CARTEIRA || G.DV_CARTEIRA,'13','0') CARTEIRA, " + 
				" g.COD_CONTRATO, " + 
				" B.NOME_BENEFICIARIO, " + 
				" CASE WHEN CB.cod_dependencia='T' THEN 'Titular' ELSE 'Dependente' END DEPENDENCIA, " + 
				" B.DATA_NASCIMENTO, " + 
				" CASE WHEN CB.cod_dependencia='D' THEN " + 
				" (SELECT b2.nome_beneficiario " + 
				" FROM beneficiario b2, contratos_beneficiario cb1 " + 
				" WHERE b2.cod_beneficiario = cb1.cod_beneficiario " + 
				" and CB.cod_unimed = cb1.cod_unimed " + 
				" and CB.cod_empresa = cb1.cod_empresa " + 
				" and CB.cod_familia = cb1.cod_familia " + 
				" and CB.cod_contrato = cb1.cod_contrato " + 
				" AND cb1.cod_dependencia = 'T' " + 
				" AND ROWNUM < 2) " + 
				" ELSE '--' " + 
				" end NOME_TITULAR, " + 
				" CASE WHEN CB.cod_dependencia='D' THEN " + 
				" (SELECT ba.DESC_PARENTESCO " + 
				" FROM parentesco ba " + 
				" WHERE 1=1 " + 
				" AND CB.COD_PARENTESCO = BA.COD_PARENTESCO " + 
				" AND ROWNUM < 2) " + 
				" ELSE '--' " + 
				" end GRAU_PARENTESCO, " + 
				" CASE " + 
				"  WHEN CB.cod_dependencia = 'D' THEN " + 
				"   (SELECT b2.COD_beneficiario " + 
				"      FROM beneficiario b2, contratos_beneficiario cb1 " + 
				"     WHERE b2.cod_beneficiario = cb1.cod_beneficiario " + 
				"       and CB.cod_unimed = cb1.cod_unimed " + 
				"       and CB.cod_empresa = cb1.cod_empresa " + 
				"       and CB.cod_familia = cb1.cod_familia " + 
				"       and CB.cod_contrato = cb1.cod_contrato " + 
				"       AND cb1.cod_dependencia = 'T' " + 
				"       AND ROWNUM < 2) " + 
				"  ELSE " + 
				"   B.COD_BENEFICIARIO " + 
				" end COD_BENEF_TITULAR, " + 
				" g.COD_HOSPITAL, P.NOME_PRESTADOR, " + 
				" G.NUM_NOTA, " + 
				" G.DATA_INTERNACAO, " + 
				" CASE  " +
				" WHEN CB.cod_dependencia = 'D' THEN (SELECT CP.cod_prestador  " +
				"                                     FROM   beneficiario b2, " + 
				" contratos_beneficiario cb1, " + 
				" compl_prestador_pf CP, " + 
				" prestador P2  " +
				"                                     WHERE  " +
				" b2.cod_beneficiario = cb1.cod_beneficiario " + 
				" AND CB.cod_unimed = cb1.cod_unimed  " +
				" AND CB.cod_empresa = cb1.cod_empresa " + 
				" AND CB.cod_familia = cb1.cod_familia  " +
				" AND CB.cod_contrato = cb1.cod_contrato " + 
				" AND cb1.cod_dependencia = 'T' " + 
				" AND CP.cpf = B2.cpf  " +
				" AND P2.cod_prestador = CP.cod_prestador " + 
				" AND P2.unimed_prestador = 63 " + 
				" AND ROWNUM < 2)  " +
				" ELSE (SELECT CP.cod_prestador  " +
				"       FROM   compl_prestador_pf CP, " + 
				"              prestador P2  " +
				"       WHERE  CP.cpf = B.cpf  " +
				"              AND P2.cod_prestador = CP.cod_prestador " + 
				"              AND P2.unimed_prestador = 63 " + 
				"              AND ROWNUM < 2)  " +
				"  END                                                       CRM, " +
              "pc.desc_plano, " +
              " cp.conselho_profissional," +
				" G.OBSERVACOES_GUIA, " + 
				" LINHAS_PARA_COLUNA('select ''( Cod.: '' || SHM.serv_med_hosp || SHM.digito_servico || '' # Desc.: '' || SA.descricao || '' # Qtd: '' || SHM.qtde_utilizada || '' )'' " + 
				" from servicos_guias_hosp SHM, SERV_MEDICOS_HOSPITALARES SA where SHM.num_nota = ' || g.num_nota || ' " + 
				" AND SA.COD_SERVICO = SHM.SERV_MED_HOSP order by 1') Servicos, " + 
				" case when g.tipo_prorrogacao = 1 " + 
				" then 'Não' " + 
				" else " + 
				" 'Sim' end as alta_administrativa " + 
				" from guias_hosp g, beneficiario b, PRESTADOR P, Contratos_Beneficiario CB, planos_contrato pc, compl_prestador_pf cp " + 
				" where g.situacao_atual not in (0,3) " + 
				" and g.data_alta is NULL " + 
				" and g.tipo_prorrogacao IN (1,3) " + 
				" and b.cod_beneficiario = g.cod_beneficiario " + 
				" and (     (G.COD_EMPRESA in (5334, 5335, 5336)) " + 
				"       or (G.COD_EMPRESA = 1902 and g.cod_contrato = 2268) " + 
				"       or (G.COD_EMPRESA = 3564 and g.cod_contrato = 2269) " + 
				"    ) " + 
				" AND P.COD_PRESTADOR = G.COD_HOSPITAL " + 
				" AND G.COD_HOSPITAL = 11005116 " + 
				" AND CB.COD_BENEFICIARIO = G.COD_BENEFICIARIO " + 
				" AND CB.COD_UNIMED = G.COD_UNIMED " + 
				" AND CB.COD_EMPRESA = G.COD_EMPRESA " + 
				" AND CB.COD_FAMILIA = G.COD_FAMILIA " + 
				" AND CB.COD_CONTRATO = G.COD_CONTRATO " + 
				" AND CB.cod_contrato = pc.cod_contrato " +
		        " AND P.cod_prestador = cp.cod_prestador " +
				" and G.COD_UNIMED = 63 " + 
				" and nvl(g.COD_UNIMED,0) > 0 " + 
				" and nvl(G.COD_EMPRESA,0) > 0 " + 
				" and CB.COD_UNIMED = 63 " + 
				" and nvl(CB.COD_UNIMED,0) > 0 " + 
				" and nvl(CB.COD_EMPRESA,0) > 0 " + 
				" and nvl(CB.COD_FAMILIA,0) > -1 " + 
				" and nvl(CB.COD_CONTRATO,0) > -1 " + 
				" and nvl(CB.COD_BENEFICIARIO,0) > 0 " + 
				") PPP " + 
				" Where 1 = 1 " + 
				" and to_date(DATA_INTERNACAO,'DD/MM/YYYY') between to_date(sysdate-2,'DD/MM/YYYY') and to_date(sysdate,'DD/MM/YYYY') ";
		
		return sql;
	}

	private List<Paciente> autorizacoesSolicitacoesCedidas(Connection conn) throws SQLException 
	{
		List<Paciente> listPaciente = new ArrayList<>();
		
		try (PreparedStatement pst = conn.prepareStatement(buildSqlSelectFromAutorizacoesSolicitacoesCedidas());
			 ResultSet rs = pst.executeQuery();)
		{
			while (rs.next())
			{
				listPaciente.add(resultSetToObjectFromAutorizacoesSolicitacoesCedidas(rs));
			}
			return listPaciente;
		}
		catch (SQLException ex)
		{
			logger.error(ex.getMessage());
			throw new SQLException(ex.getMessage());
		}
	}
	
	public Paciente resultSetToObjectFromAutorizacoesSolicitacoesCedidas(ResultSet rs) throws SQLException
	{
		int i = 1;
		Paciente paciente = new Paciente();
		
		paciente.setNrCarteira(rs.getString(i++));
		paciente.setNmPaciente(rs.getString(i++));
		paciente.setDtNascimento(rs.getDate(i++));
		paciente.setDtAtendimento(rs.getString(i++));
		paciente.setCdUf(rs.getString(i++));
		paciente.setCdPrestador(new Integer(rs.getString(i++)));
		paciente.setDsConPlan(rs.getString(i++));
		paciente.setDsCodigoConselho(new Integer(rs.getString(i++)));
		paciente.setNrFone(rs.getString(i++));
		paciente.setPrioridade(new Integer(1));
		
		return paciente;
	}
	
	private String buildSqlSelectFromAutorizacoesSolicitacoesCedidas()
	{
		String sql = " select LPAD(cb.unimed_carteira, '3', '0') || LPAD(cb.cod_carteira || cb.dv_carteira,'13','0') as carteira, " + 
				"        b.nome_beneficiario, " +
				"        b.DATA_NASCIMENTO, " + 
				"        g.data_internacao, " +
				"        t.cod_requisitante as crm, " +
				"        t.cod_prestador, " +
				"        pc.desc_plano, " +
				"        cp.conselho_profissional, " +
				"        NVL((select distinct RPAD(nvl(mc2.numero_contato,' '), 11, ' ') " +
				"             from enderecos_prestador ep2, meio_contato mc2 " +
				"             where ep2.cod_prestador(+) = t.cod_requisitante " +
				"               and mc2.cod_endereco(+) = ep2.cod_endereco " +
				"               and mc2.cod_tipo_meio_contato = 5 " +
				"               and ROWNUM < 2),'--') AS CELULAR_TITULAR " +
				"   from transacoes                t, " + 
				"        carteira_beneficiario     cb, " + 
				"        beneficiario              b, " + 
				"        servicos_transacoes       st, " + 
				"        serv_medicos_hospitalares smh, " + 
				"        guias_hosp                g, " + 
				"        guias_peq_cirurgias       gpc, " + 
				"        prestador                 p1, " + 
				"        prestador                 p2, " + 
				"        contratos_beneficiario    cbc, " +
				"        planos_contrato pc, " +
				"        compl_prestador_pf cp, " +
				"        lotacoes                   l, " + 
				"        familias                   f " + 
				"  where t.unimed_cartao_prest = cb.unimed_carteira " + 
				"    and t.cod_carteira = cb.cod_carteira " + 
				"    and t.dv_carteira = cb.dv_carteira " + 
				"    and cb.cod_beneficiario = b.cod_beneficiario " + 
				"    and nvl(t.num_transacao,-1) > -1 " + 
				"    and t.data_transacao >= sysdate - 1 " + 
				"    and t.cod_unimed_prest = 63 " + 
				"    and t.tipo_nota in (10, 5) " + 
				"    and t.tipo_transacao IN (1, 5) " + 
				"    and cb.cod_contrato in (4654, 20) " + 
				"    and t.num_transacao = st.num_transacao " + 
				"    and st.result_transacao = 1 " + 
				"    and st.qtde_serv_med_hosp > 0 " + 
				"    and st.cod_serv_med_hosp = smh.cod_servico " + 
				"    and st.dv_serv_med_hosp = smh.dv_servico " + 
				"    and t.num_nota = g.num_nota(+) " + 
				"    AND G.TIPO_PRORROGACAO <> 2 " + 
				"    and t.num_nota = gpc.num_nota(+) " + 
				"    and p1.cod_prestador = t.cod_requisitante " + 
				"    and p2.cod_prestador = t.cod_prestador " +
				"    and p1.cod_prestador = cp.cod_prestador " +
				"    and cb.cod_beneficiario = cbc.cod_beneficiario " +
				"    and pc.cod_contrato = cbc.cod_contrato " +
				"    and cb.cod_unimed = cbc.cod_unimed " + 
				"    and cb.cod_contrato = cbc.cod_contrato " + 
				"    and cb.cod_familia = cbc.cod_familia " + 
				"    and cb.cod_empresa = cbc.cod_empresa " + 
				"    and f.cod_unimed = cb.cod_unimed " + 
				"            and f.cod_empresa = cb.cod_empresa " + 
				"            and f.cod_familia = cb.cod_familia " + 
				"            and nvl(f.data_excl, sysdate) >= sysdate " + 
				"            and l.cod_unimed = f.cod_unimed " + 
				"            and l.cod_empresa = f.Cod_Empresa " + 
				"            and l.cod_lotacao = f.cod_lotacao " + 
				"    group by b.nome_beneficiario, " + 
				"          cb.unimed_carteira, " + 
				"          cb.cod_carteira, " + 
				"          cb.dv_carteira, " + 
				"          cb.cod_contrato, " + 
				"          t.num_nota, " + 
				"          t.cod_requisitante, " + 
				"          p1.nome_prestador, " +
				"          cp.conselho_profissional, " +
				"          b.data_nascimento, " + 
				"          cbc.cod_dependencia, " + 
				"          l.desc_lotacao, " + 
				"          t.cod_prestador, " +
				"          pc.desc_plano, " +
				"          p2.nome_prestador, " + 
				"          t.obs_justificativa, " + 
				"          cb.cod_unimed, " + 
				"          cb.cod_empresa, " + 
				"          cb.cod_familia, " + 
				"          g.data_internacao, " + 
				"          gpc.data_atendimento " + 
				"ORDER BY 1 ";
		
		return sql;
	}
	
	private List<Paciente> correcaoGeracaoDiasUteis(Connection conn) throws SQLException 
	{
		List<Paciente> listPaciente = new ArrayList<>();
		
		try (PreparedStatement pst = conn.prepareStatement(buildSqlSelectGeradorProcessamento());
			 ResultSet rs = pst.executeQuery();)
		{
			if (rs.next())
			{
				List<GeradorProcessamento> listGeradorProcessamento = new ArrayList<>();
				listGeradorProcessamento.add(resultSetToObjectFromGeradorProcessamento(rs));
				
				if (listGeradorProcessamento != null
						&& listGeradorProcessamento.size() > 0)
				{
					for (GeradorProcessamento element : listGeradorProcessamento)
					{
						if (element.getStatus() != null
								&& !element.getStatus().equalsIgnoreCase(""))
						{
							String dataUtilAtualStr = DateUtils.retornaDataUtil(1);
							String proximaDataUtilStr = DateUtils.retornaDataUtil(2);
							
							if (element.getStatus().equalsIgnoreCase("A"))
							{
								if (element.getDataAgendamento() != null)
								{
									String dataAgendamentoStr = DateUtils.convertToLocalDateViaInstant(element.getDataAgendamento());
									
									if (!dataAgendamentoStr.equalsIgnoreCase(dataUtilAtualStr))
									{
										
									}
								}
							}
							
							if (element.getStatus().equalsIgnoreCase("I"))
							{
								
							}
						}
					}
				}
//				listPaciente.add(resultSetToObjectFromGeradorProcessamento(rs));
			}
			return listPaciente;
		}
		catch (SQLException ex)
		{
			logger.error(ex.getMessage());
			throw new SQLException(ex.getMessage());
		}
	}
	
	private String buildSqlSelectGeradorProcessamento()
	{
		String sql = " SELECT DISTINCT GP.COD_RELATORIO, " + 
				"                GP.COD_PROCESSAMENTO, " + 
				"                GP.STATUS, " + 
				"                TO_DATE(TO_CHAR(GP.DATA_AGENDAMENTO, 'DD/MM/YYYY'), " + 
				"                        'DD/MM/YYYY') DATA_AGENDAMENTO, " + 
				"                TO_CHAR(GP.DATA_AGENDAMENTO, 'HH24') HORA, " + 
				"                TO_CHAR(GP.DATA_AGENDAMENTO, 'MI') MINUTO, " + 
				"                TO_CHAR(GP.DATA_AGENDAMENTO, 'SS') SEGUNDO " + 
				"  FROM GERADOR_PROCESSAMENTO GP " + 
				" WHERE GP.COD_RELATORIO in (3002, 2999, 2998, 3000, 3001, 3006) " + 
				"   AND TO_DATE(TO_CHAR(GP.DATA_AGENDAMENTO, 'DD/MM/YYYY'), 'DD/MM/YYYY') = " + 
				"       TO_DATE(TO_CHAR(SYSDATE, 'DD/MM/YYYY'), 'DD/MM/YYYY') + 1 " + 
				"   AND GP.STATUS = 'A' " + 
				"   AND GP.EMAIL <> 'alexandre.amorim@unimedfortaleza.com.br' ";
		
		return sql;
	}
	
	public GeradorProcessamento resultSetToObjectFromGeradorProcessamento(ResultSet rs) throws SQLException
	{
		int i = 1;
		GeradorProcessamento geradorProcessamento = new GeradorProcessamento();
		
		geradorProcessamento.setCdRelatorio(new Integer(rs.getString(i++)));
		geradorProcessamento.setCdProcessamento(new Integer(rs.getString(i++)));
		geradorProcessamento.setDataAgendamento(rs.getDate(i++));
		geradorProcessamento.setHora(rs.getString(i++));
		geradorProcessamento.setMinuto(rs.getString(i++));
		geradorProcessamento.setSegundo(rs.getString(i++));
		
		return geradorProcessamento;
	}
}