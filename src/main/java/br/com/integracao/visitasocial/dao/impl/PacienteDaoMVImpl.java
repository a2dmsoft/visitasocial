package br.com.integracao.visitasocial.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.integracao.visitasocial.dao.PacienteDaoMV;
import br.com.integracao.visitasocial.exceptions.AppException;
import br.com.integracao.visitasocial.factory.ConnectionFactory;
import br.com.integracao.visitasocial.model.Paciente;
import br.com.integracao.visitasocial.utils.ConnectionUtils;

public class PacienteDaoMVImpl implements PacienteDaoMV
{
	static Logger logger = Logger.getLogger(PacienteDaoMVImpl.class);
	
	@Override
	public List<Paciente> obterDados() throws Exception
	{
		logger.info("Obtendo dados MV");
		ConnectionFactory instanceConn = ConnectionUtils.configConnMV();
		List<Paciente> listVisitaSocial = new ArrayList<>();
		
		try (Connection conn = instanceConn.createConnection();
			 PreparedStatement pst = conn.prepareStatement(buildSqlSelect());
			 ResultSet rs = pst.executeQuery())
		{
			while (rs.next())
			{
				listVisitaSocial.add(resultSetToObject(rs));
			}
			logger.info("Dados do MV obtidos com sucesso");
			return listVisitaSocial;
		}
		catch (SQLException ex)
		{
			logger.error(ex.getMessage());
			throw new AppException(ex.getMessage());
		}
	}
	
	public Paciente resultSetToObject(ResultSet rs) throws SQLException
	{
		int i = 1;
		Paciente paciente = new Paciente();
		
		paciente.setNrCarteira(rs.getString(i++));
		paciente.setNmPaciente(rs.getString(i++));
		paciente.setDtNascimento(rs.getDate(i++));
		paciente.setNrFone(rs.getString(i++));
		paciente.setDsConPlan(rs.getString(i++));
		paciente.setCdLeito(new Integer(rs.getString(i++)));
		paciente.setDsLeito(rs.getString(i++));
		paciente.setDtAtendimento(rs.getString(i++));
		paciente.setCdUf(rs.getString(i++));
		
		String cdMotAltaStr = rs.getString(i++);
		
		if (cdMotAltaStr != null
				&& !cdMotAltaStr.equalsIgnoreCase(""))
		{
			paciente.setCdMotAlt(new Integer(cdMotAltaStr));
		}
		
		paciente.setDtAlta(rs.getString(i++));
		paciente.setPrioridade(new Integer(0));
		
		return paciente;
	}
	
	private String buildSqlSelect()
	{
		String sql = " SELECT A.NR_CARTEIRA, " + 
				"       P.NM_PACIENTE, " + 
				"       P.DT_NASCIMENTO, " + 
				"       P.NR_FONE, " + 
				"       con_pla.ds_con_pla, " +
				"       L.CD_LEITO, " +
				"       L.DS_LEITO, " + 
				"       A.DT_ATENDIMENTO, " + 
				"       conselho.cd_uf, " + 
				"       A.cd_mot_alt, " + 
				"       A.DT_ALTA_MEDICA " + 
				"  FROM ATENDIME A, LEITO L, UNID_INT U, PACIENTE P, con_pla, conselho, prestador " + 
				"WHERE A.CD_MULTI_EMPRESA = 1 " + 
				"   AND A.TP_ATENDIMENTO = 'I' " + 
				"   AND A.DT_ALTA IS NULL " + 
				"   AND A.CD_LEITO = L.CD_LEITO " + 
				"   AND L.CD_UNID_INT = U.CD_UNID_INT " + 
				"   AND A.CD_PACIENTE = P.CD_PACIENTE " + 
				"   AND A.cd_con_pla = con_pla.cd_con_pla  " + 
				"   AND A.cd_convenio = con_pla.cd_convenio  " + 
				"   AND A.cd_prestador = prestador.cd_prestador  " + 
				"   AND prestador.cd_conselho = conselho.cd_conselho  " + 
				"ORDER BY U.DS_UNID_INT, L.DS_ENFERMARIA ";
		
		return sql;
	}
}