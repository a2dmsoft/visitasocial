package br.com.integracao.visitasocial.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import br.com.integracao.visitasocial.dao.CarteiraBeneficiarioDao;
import br.com.integracao.visitasocial.model.CarteiraBeneficiario;

public class CarteiraBeneficiarioDaoImpl implements CarteiraBeneficiarioDao
{
	static Logger logger = Logger.getLogger(CarteiraBeneficiarioDaoImpl.class);
	
	@Override
	public CarteiraBeneficiario pesquisar(Connection conn, Long codCarteira) throws SQLException
	{
		CarteiraBeneficiario carteiraBeneficiario = null;
		
		try (PreparedStatement pst = conn.prepareStatement(buildSqlSelect(codCarteira));
			 ResultSet rs = pst.executeQuery();)
		{
			if (rs.next())
			{
				carteiraBeneficiario = new CarteiraBeneficiario();
				carteiraBeneficiario.setCodContrato(new Integer(rs.getString(1)));
				carteiraBeneficiario.setCodDependencia(rs.getString(2));
			}
			
			return carteiraBeneficiario;
		}
		catch (SQLException ex)
		{
			logger.error(ex.getMessage());
			throw new SQLException(ex.getMessage());
		}
	}
	
	private String buildSqlSelect(Long codCarteira)
	{
		String sql = " SELECT cart.cod_contrato, cont.cod_dependencia "
				   + " FROM sabius.CARTEIRA_BENEFICIARIO cart "
				   + " INNER JOIN sabius.CONTRATOS_BENEFICIARIO cont on cart.cod_beneficiario = cont.cod_beneficiario "
				   + "                                               and cart.cod_unimed = cont.cod_unimed "
				   + "                                               and cart.cod_empresa = cont.cod_empresa "
				   + "                                               and cart.cod_familia = cont.cod_familia "
				   + "                                               and cart.cod_contrato = cont.cod_contrato " 
				   + " WHERE cart.cod_carteira = " + codCarteira;
		
		return sql;
	}
	
	@Override
	public String pesquisarCodPlano(Connection conn, Integer unimedCarteira, Long codCarteira, String dvCarteira) throws SQLException
	{
		try (PreparedStatement pst = conn.prepareStatement(buildSqlSelectCodPlano(unimedCarteira, codCarteira, dvCarteira));
			 ResultSet rs = pst.executeQuery();)
		{
			if (rs.next())
			{
				return rs.getString(1);
			}
			
			return "";
		}
		catch (SQLException ex)
		{
			logger.error(ex.getMessage());
			throw new SQLException(ex.getMessage());
		}
	}

	public String buildSqlSelectCodPlano(Integer unimedCarteira, Long codCarteira, String dvCarteira) 
	{
		String sql = " SELECT htb.cod_plano " + 
					"FROM carteira_beneficiario cab " + 
					",hist_transferencia_benef htb " + 
					"WHERE 1 = 1 " + 
					"AND cab.cod_beneficiario = htb.cod_beneficiario " + 
					"AND cab.cod_unimed = htb.cod_unimed " + 
					"AND cab.cod_empresa = htb.cod_empresa " + 
					"AND cab.cod_contrato = htb.cod_contrato " + 
					"AND cab.cod_familia = htb.cod_familia " + 
					"and htb.data_referencia = (select max(htb1.data_referencia) from hist_transferencia_benef htb1 " + 
					"where htb1.cod_beneficiario = htb.cod_beneficiario " + 
					"AND htb1.cod_unimed = htb.cod_unimed " + 
					"AND htb1.cod_empresa = htb.cod_empresa " + 
					"AND htb1.cod_contrato = htb.cod_contrato " + 
					"AND htb1.cod_familia = htb.cod_familia " + 
					"and htb1.data_referencia <= SYSDATE " + 
					") " + 
					"AND cab.cod_carteira = " + codCarteira +
					" and cab.dv_carteira = " + dvCarteira +
					" and cab.unimed_carteira = " + unimedCarteira;
		
		return sql;
	}
}