package br.com.integracao.visitasocial.dao.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.integracao.visitasocial.constants.BaseConstants;
import br.com.integracao.visitasocial.dao.VisitaSocialDao;
import br.com.integracao.visitasocial.exceptions.AppException;
import br.com.integracao.visitasocial.factory.ConnectionFactory;
import br.com.integracao.visitasocial.interceptor.BasicAuthInterceptor;
import br.com.integracao.visitasocial.model.CarteiraBeneficiario;
import br.com.integracao.visitasocial.model.IntegracaoVisitaSocial;
import br.com.integracao.visitasocial.model.Objeto;
import br.com.integracao.visitasocial.model.Paciente;
import br.com.integracao.visitasocial.model.Prestador;
import br.com.integracao.visitasocial.model.TipoSaida;
import br.com.integracao.visitasocial.model.UfCrm;
import br.com.integracao.visitasocial.model.UnimedProducao;
import br.com.integracao.visitasocial.model.Unimeds;
import br.com.integracao.visitasocial.model.Usuario;
import br.com.integracao.visitasocial.model.VisitaSocial;
import br.com.integracao.visitasocial.model.VisitaSocialRetorno;
import br.com.integracao.visitasocial.model.VisitaSocialRetornoListar;
import br.com.integracao.visitasocial.request.ApiRequest;
import br.com.integracao.visitasocial.request.VisitaSocialRequest;
import br.com.integracao.visitasocial.response.VisitaSocialResponse;
import br.com.integracao.visitasocial.response.VisitaSocialResponseListar;
import br.com.integracao.visitasocial.utils.ConnectionUtils;
import br.com.integracao.visitasocial.utils.DateUtils;
import br.com.integracao.visitasocial.utils.IntegracaoUtils;

public class VisitaSocialDaoImpl implements VisitaSocialDao
{
	static Logger logger = Logger.getLogger(VisitaSocialDaoImpl.class);
	
	@Override
	public VisitaSocialResponse inserir(Paciente pacienteMV) throws JsonProcessingException
	{
		VisitaSocialResponse visitaSocialResponse = new VisitaSocialResponse();
		RestTemplate restTemplate = new RestTemplate();
		
		BasicAuthInterceptor basicAuthInterceptor = new BasicAuthInterceptor(ConnectionUtils.getUser(), ConnectionUtils.getPassword());
		
		List<ClientHttpRequestInterceptor> interceptors = new ArrayList<ClientHttpRequestInterceptor>();
        interceptors.add(basicAuthInterceptor);
        restTemplate.setInterceptors(interceptors);
        
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
		
        VisitaSocialRequest visitaSocialRequest = instanciarObjetoParaInserir(pacienteMV);
        
        if (!IntegracaoUtils.isPacienteIntercambio(visitaSocialRequest.getIntegracaoVisitaSocial().getVisitaSocial().getUnimedCarteira()))
        {
        	VisitaSocialRetornoListar elementAlterar = null;
        	VisitaSocial visitaSocial = new VisitaSocial(new UnimedProducao(new Unimeds(IntegracaoUtils.UNIMED_PRODUCAO)),
        												 63,
        			                                     new Long(2005468813),
        			                                     "2");
        	
        	IntegracaoVisitaSocial integracaoVisitaSocial = new IntegracaoVisitaSocial(visitaSocial);
        	Objeto objetoListar = new Objeto(ConnectionUtils.getUriListar(), new VisitaSocialRequest(integracaoVisitaSocial), IntegracaoUtils.POST, basicAuthInterceptor.getAuthHeader());
        	
        	ApiRequest apiRequest = new ApiRequest(objetoListar);
        	HttpEntity<ApiRequest> entity = new HttpEntity<ApiRequest>(apiRequest, headers);
        	entity = new HttpEntity<ApiRequest>(apiRequest, headers);
        	String json = new ObjectMapper().writeValueAsString(apiRequest);
        	
        	VisitaSocialResponseListar visitaSocialResponseListar = restTemplate.postForObject(IntegracaoUtils.URL_OSB, entity, VisitaSocialResponseListar.class);
        	
        	if (visitaSocialResponseListar != null
        			&& visitaSocialResponseListar.getRetorno() != null
    				&& visitaSocialResponseListar.getRetorno().size() > 0)
    		{
        		Boolean existeRegistroEmAberto = false;
        		
        		for (VisitaSocialRetornoListar element : visitaSocialResponseListar.getRetorno()) 
        		{
        			if (element.getVsVisitaSocial().getDataAlta() == null) 
        			{
        				existeRegistroEmAberto = true;
        				elementAlterar = element;
        				break;
        			}
        		}
        		
        		if (!existeRegistroEmAberto
        				&& !IntegracaoUtils.isPacientePrioridade(visitaSocialRequest.getIntegracaoVisitaSocial().getVisitaSocial().getPrioridade())) 
        		{
        			Objeto objetoInserir = new Objeto(ConnectionUtils.getUri(), visitaSocialRequest, IntegracaoUtils.POST, basicAuthInterceptor.getAuthHeader());
    				ApiRequest apiRequestInserir = new ApiRequest(objetoInserir);
    				entity = new HttpEntity<ApiRequest>(apiRequestInserir, headers);
    				visitaSocialResponse = restTemplate.postForObject(IntegracaoUtils.URL_OSB, entity, VisitaSocialResponse.class);
        		}
        		else if (IntegracaoUtils.isPacientePrioridade(visitaSocialRequest.getIntegracaoVisitaSocial().getVisitaSocial().getPrioridade()))
        		{
        			elementAlterar.getVsVisitaSocial().setPrioridade(1);
    				IntegracaoVisitaSocial integracaoVisitaSocialAlterar = new IntegracaoVisitaSocial(elementAlterar.getVsVisitaSocial());
    		        VisitaSocialRequest request = new VisitaSocialRequest(integracaoVisitaSocialAlterar);
    				
    				Objeto objetoAlterar = new Objeto(ConnectionUtils.getUriAlterar(), request, IntegracaoUtils.POST, basicAuthInterceptor.getAuthHeader());
    				ApiRequest apiRequestAlterar = new ApiRequest(objetoAlterar);
    				entity = new HttpEntity<ApiRequest>(apiRequestAlterar, headers);
    				visitaSocialResponse = restTemplate.postForObject(IntegracaoUtils.URL_OSB, entity, VisitaSocialResponse.class);
        		}
        		else
    			{
    				visitaSocialResponse.setSucesso(false);
    				visitaSocialResponse.setMensagem(BaseConstants.REGISTRO_PACIENTE_ABERTO_NAO_PRIORITARIO);
    				visitaSocialResponse.setRetorno(buildVisitaSocialRetorno(visitaSocialRequest));
    			}
    		}
        	else
        	{
        		Objeto objetoInserir = new Objeto(ConnectionUtils.getUri(), visitaSocialRequest, IntegracaoUtils.POST, basicAuthInterceptor.getAuthHeader());
        		ApiRequest apiRequestInserir = new ApiRequest(objetoInserir);
        		entity = new HttpEntity<ApiRequest>(apiRequestInserir, headers);
        		visitaSocialResponse = restTemplate.postForObject(IntegracaoUtils.URL_OSB, entity, VisitaSocialResponse.class);
        	}
        }
        else
        {
        	visitaSocialResponse.setSucesso(false);
        	visitaSocialResponse.setMensagem(BaseConstants.PACIENTE_INTERCAMBIO_ERROR);
        	visitaSocialResponse.setRetorno(buildVisitaSocialRetorno(visitaSocialRequest));
        }
        
        return visitaSocialResponse;
	}

	private VisitaSocialRetorno buildVisitaSocialRetorno(VisitaSocialRequest visitaSocialRequest) 
	{
		VisitaSocial visitaSocial = visitaSocialRequest.getIntegracaoVisitaSocial().getVisitaSocial();
		
		String ufCrm = "";
		if (visitaSocial.getUfCrm() != null) {
			ufCrm = visitaSocial.getUfCrm().getCodUf();
		}
		
		return new VisitaSocialRetorno(visitaSocial.getIdVisitaSocial(), visitaSocial.getDataCadastro(), visitaSocial.getUsuario(),
				                       visitaSocial.getUnimedProducao(), visitaSocial.getMotivoAlta(), visitaSocial.getUnimedCarteira(), visitaSocial.getCodCarteira(), visitaSocial.getDvCarteira(),
				                       visitaSocial.getPaciente(), visitaSocial.getIdade(), visitaSocial.getTelefone(), visitaSocial.getCodPlano(), visitaSocial.getPrestador(), visitaSocial.getLeito(), visitaSocial.getDescLeito(),
				                       visitaSocial.getDataInternacao(), visitaSocial.getDataAlta(), visitaSocial.getCrm(), visitaSocial.getAdesaoMedPrev(), visitaSocial.getPerfilMedPrev(), new UfCrm(ufCrm),
				                       visitaSocial.getCodContrato(), visitaSocial.getCodDependencia(), visitaSocial.getPrioridade());
	}

	private VisitaSocialRequest instanciarObjetoParaInserir(Paciente paciente) 
	{
		Integer unimedCarteira = null;
		Long codCarteira = null;
		String dvCarteira = null;
		
		if (paciente != null
				&& paciente.getNrCarteira() != null
				&& !paciente.getNrCarteira().equalsIgnoreCase(""))
		{
			paciente.setNrCarteira(nrCarteiraFormatted(paciente.getNrCarteira()));
			
			unimedCarteira = new Integer(paciente.getNrCarteira().substring(0, 3));
			codCarteira = new Long(paciente.getNrCarteira().substring(3, paciente.getNrCarteira().length()-1));
			dvCarteira = paciente.getNrCarteira().substring(paciente.getNrCarteira().length()-1);
		}
		
		ConnectionFactory instanceConn = ConnectionUtils.configConnSabius();
		VisitaSocial visitaSocial = new VisitaSocial();
		VisitaSocialRequest visitaSocialRequest = new VisitaSocialRequest();
		
		try (Connection conn = instanceConn.createConnection())
		{
			CarteiraBeneficiario carteiraBeneficiario = new CarteiraBeneficiarioDaoImpl().pesquisar(conn, codCarteira);
			
			visitaSocial.setDataCadastro(DateUtils.removerHoraByDate(new Date()));
			visitaSocial.setUsuario(new Usuario(IntegracaoUtils.USER));
			visitaSocial.setUnimedProducao(new UnimedProducao(new Unimeds(IntegracaoUtils.UNIMED_PRODUCAO)));
			visitaSocial.setUnimedCarteira(unimedCarteira);
			visitaSocial.setCodCarteira(codCarteira);
			visitaSocial.setDvCarteira(dvCarteira);
			visitaSocial.setPaciente(paciente.getNmPaciente());
			visitaSocial.setIdade(DateUtils.calculaIdade(paciente.getDtNascimento()));
			visitaSocial.setTelefone(paciente.getNrFone());
			visitaSocial.setPrestador(new Prestador(11005116));
			visitaSocial.setDataInternacao(DateUtils.removerHoraByDateStr(paciente.getDtAtendimento()));
			visitaSocial.setAdesaoMedPrev("N");
			visitaSocial.setPerfilMedPrev("N");
			if (paciente.getCdUf() != null
					&& !paciente.getCdUf().equalsIgnoreCase("")) {
				visitaSocial.setUfCrm(new UfCrm(paciente.getCdUf()));
			}
			visitaSocial.setPrioridade(paciente.getPrioridade());
			visitaSocial.setCodPlano(new CarteiraBeneficiarioDaoImpl().pesquisarCodPlano(conn, unimedCarteira, codCarteira, dvCarteira));
			visitaSocial.setMotivoAlta(new TipoSaida(IntegracaoUtils.getMapTipoSaida(paciente.getCdMotAlt())));
			visitaSocial.setDataAlta(DateUtils.removerHoraByDateStr(paciente.getDtAlta()));

			if (paciente.getCdLeito() != null)
			{
				visitaSocial.setLeito(paciente.getCdLeito());
				visitaSocial.setDescLeito(paciente.getDsLeito());
			}
			else 
			{
				visitaSocial.setDescLeito("");
			}
			
			if (existePrestador(paciente, conn))
			{
				visitaSocial.setCrm(paciente.getDsCodigoConselho());
			}
			
			if (carteiraBeneficiario != null)
			{
				visitaSocial.setCodContrato(carteiraBeneficiario.getCodContrato());
				visitaSocial.setCodDependencia(carteiraBeneficiario.getCodDependencia());
			}
			
			visitaSocialRequest = new VisitaSocialRequest(new IntegracaoVisitaSocial(visitaSocial));
		}
		catch (Exception ex)
		{
			logger.error(ex.getMessage());
			throw new AppException(ex.getMessage());
		}
		
		logger.info(visitaSocial.toString());
		
		return visitaSocialRequest;
	}

	private Boolean existePrestador(Paciente paciente, Connection conn) throws SQLException 
	{
		Integer codigoPrestador = new PrestadorDaoImpl().pesquisarCodigoPrestadorPorCrm(conn, paciente.getDsCodigoConselho());
		return codigoPrestador != null && codigoPrestador.intValue() > 0;
	}

	private String nrCarteiraFormatted(String nrCarteira) 
	{
		return nrCarteira.replace("-", "").replace(" ", "").replace(".", "");
	}
}