package br.com.integracao.visitasocial.execute;

import org.apache.log4j.Logger;

import br.com.integracao.visitasocial.service.IntegracaoService;
import br.com.integracao.visitasocial.utils.ConnectionUtils;
import br.com.integracao.visitasocial.utils.FileUtils;

public class ProcessarIntegracao
{
	static Logger logger = Logger.getLogger(ProcessarIntegracao.class);
	
	private static String uri;
	
	private static String uriAlterar;
	
	private static String uriListar;
	
	private static String user;
	
	private static String password;
	
	private static String hostDb;
	
	private static String serviceDb;
	
	private static String portDb;
	
	private static String userDb;
	
	private static String passwordDb;
	
	private static String hostSabiusDb;
	
	private static String serviceSabiusDb;
	
	private static String portSabiusDb;
	
	private static String userSabiusDb;
	
	private static String passwordSabiusDb;
	
	public static void main(String args[])
	{
		try {
		
			EraseFile();

			logger.info("Integração iniciada");
			
			initVariables();
			
			if (validation())
			{
				process();
			}
			logger.info("Integração finalizada");
			
			
		} catch (Exception e) {
			
			logger.error("Erro ao executar operação", e);
		}
		
	}
	
	private static void process() throws Exception 
	{
		logger.info("Processamento iniciado");
		new IntegracaoService().processar();
		logger.info("Processamento finalizado");
	}

	private static void EraseFile()
	{
		logger.info("Limpado o arquivo de log");
		FileUtils.apagarConteudo();
	}
	
	private static void initVariables()
	{
//		System.setProperty("uri", "http://tf1jboss.unimedfortaleza.com.br:8081/sabius-servicos-web/rest-servicos/visitaSocial/incluir");
//		System.setProperty("urialterar", "http://tf1jboss.unimedfortaleza.com.br:8081/sabius-servicos-web/rest-servicos/visitaSocial/alterar");
//		System.setProperty("urilistar", "http://tf1jboss.unimedfortaleza.com.br:8081/sabius-servicos-web/rest-servicos/visitaSocial/listar");
		
//		System.setProperty("uri", "http://pjweb.unimedfortaleza.com.br/sabius-servicos-web/rest-servicos/visitaSocial/incluir");
//		System.setProperty("urialterar", "http://pjweb.unimedfortaleza.com.br/sabius-servicos-web/rest-servicos/visitaSocial/alterar");
//		System.setProperty("urilistar", "http://pjweb.unimedfortaleza.com.br/sabius-servicos-web/rest-servicos/visitaSocial/listar");
//		System.setProperty("user", "AUDSOCIAL");
//		System.setProperty("password", "au2011");
//		System.setProperty("hostdb", "racmv.unimedfortaleza.com.br");
//		System.setProperty("servicedb", "mv");
//		System.setProperty("portdb", "1528");
//		System.setProperty("userdb", "reader");
//		System.setProperty("passworddb", "reader");
//		System.setProperty("hostsabiusdb", "racsab.unimedfortaleza.com.br");
//		System.setProperty("servicesabiusdb", "sab");
//		System.setProperty("portsabiusdb", "1522");
//		System.setProperty("usersabiusdb", "reader");
//		System.setProperty("passwordsabiusdb", "reader");
		
		logger.info("Inicializando parâmetros");
		
		uri = ConnectionUtils.getUri();
		uriAlterar = ConnectionUtils.getUriAlterar();
		uriListar = ConnectionUtils.getUriListar();
		user = ConnectionUtils.getUser();
		password = ConnectionUtils.getPassword();
		hostDb = ConnectionUtils.getHostDb();
		serviceDb = ConnectionUtils.getServiceDb();
		portDb = ConnectionUtils.getPortDb();
		userDb = ConnectionUtils.getUserDb();
		passwordDb = ConnectionUtils.getPasswordDb();
		hostSabiusDb = ConnectionUtils.getHostSabiusDb();
		serviceSabiusDb = ConnectionUtils.getServiceSabiusDb();
		portSabiusDb = ConnectionUtils.getPortSabiusDb();
		userSabiusDb = ConnectionUtils.getUserSabiusDb();
		passwordSabiusDb = ConnectionUtils.getPasswordSabiusDb();
	}
	
	private static Boolean validation() 
	{
		if (uri == null 
				|| uri.equalsIgnoreCase(""))
		{
			logger.info("É necessário informar o campo: -Duri");
			return false;
		}
		
		if (uriAlterar == null 
				|| uriAlterar.equalsIgnoreCase(""))
		{
			logger.info("É necessário informar o campo: -Durialterar");
			return false;
		}
		
		if (uriListar == null 
				|| uriListar.equalsIgnoreCase(""))
		{
			logger.info("É necessário informar o campo: -Durilistar");
			return false;
		}
		
		if (user == null 
				|| user.equalsIgnoreCase(""))
		{
			logger.info("É necessário informar o campo: -Duser");
			return false;
		}
		
		if (password == null 
				|| password.equalsIgnoreCase(""))
		{
			logger.info("É necessário informar o campo: -Dpassword");
			return false;
		}
		
		if (hostDb == null 
				|| hostDb.equalsIgnoreCase(""))
		{
			logger.info("É necessário informar o campo: -Dhostdb");
			return false;
		}
		
		if (serviceDb == null 
				|| serviceDb.equalsIgnoreCase(""))
		{
			logger.info("É necessário informar o campo: -Dservicedb");
			return false;
		}
		
		if (portDb == null 
				|| portDb.equalsIgnoreCase(""))
		{
			logger.info("É necessário informar o campo: -Dportdb");
			return false;
		}
		
		if (userDb == null 
				|| userDb.equalsIgnoreCase(""))
		{
			logger.info("É necessário informar o campo: -Duserdb");
			return false;
		}
		
		if (passwordDb == null 
				|| passwordDb.equalsIgnoreCase(""))
		{
			logger.info("É necessário informar o campo: -Dpassworddb");
			return false;
		}
		
		if (hostSabiusDb == null 
				|| hostSabiusDb.equalsIgnoreCase(""))
		{
			logger.info("É necessário informar o campo: -Dhostsabiusdb");
			return false;
		}
		
		if (serviceSabiusDb == null 
				|| serviceSabiusDb.equalsIgnoreCase(""))
		{
			logger.info("É necessário informar o campo: -Dservicesabiusdb");
			return false;
		}
		
		if (portSabiusDb == null 
				|| portSabiusDb.equalsIgnoreCase(""))
		{
			logger.info("É necessário informar o campo: -Dportsabiusdb");
			return false;
		}
		
		if (userSabiusDb == null 
				|| userSabiusDb.equalsIgnoreCase(""))
		{
			logger.info("É necessário informar o campo: -Dusersabiusdb");
			return false;
		}
		
		if (passwordSabiusDb == null 
				|| passwordSabiusDb.equalsIgnoreCase(""))
		{
			logger.info("É necessário informar o campo: -Dpasswordsabiusdb");
			return false;
		}
		
		return true;
	}
}