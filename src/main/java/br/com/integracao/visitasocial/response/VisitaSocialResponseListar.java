package br.com.integracao.visitasocial.response;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import br.com.integracao.visitasocial.model.VisitaSocialRetornoListar;

@XmlRootElement
public class VisitaSocialResponseListar implements Serializable {

	private static final long serialVersionUID = -3389333209713569083L;

	private Boolean sucesso;

	private List<VisitaSocialRetornoListar> retorno;
	
	private String mensagem;

	public Boolean getSucesso() {
		return sucesso;
	}

	public void setSucesso(Boolean sucesso) {
		this.sucesso = sucesso;
	}

	public List<VisitaSocialRetornoListar> getRetorno() {
		return retorno;
	}

	public void setRetorno(List<VisitaSocialRetornoListar> retorno) {
		this.retorno = retorno;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
}