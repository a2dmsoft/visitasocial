package br.com.integracao.visitasocial.utils;

import br.com.integracao.visitasocial.factory.ConnectionFactory;

public class ConnectionUtils {
	
	private static String uri;

	private static String uriAlterar;

	private static String uriListar;

	private static String user;

	private static String password;

	private static String hostDb;

	private static String serviceDb;

	private static String portDb;

	private static String userDb;

	private static String passwordDb;

	private static String hostSabiusDb;

	private static String serviceSabiusDb;

	private static String portSabiusDb;

	private static String userSabiusDb;

	private static String passwordSabiusDb;

	static 
	{
		uri = System.getProperty("uri");
		uriAlterar = System.getProperty("urialterar");
		uriListar = System.getProperty("urilistar");
		user = System.getProperty("user");
		password = System.getProperty("password");
		hostDb = System.getProperty("hostdb");
		serviceDb = System.getProperty("servicedb");
		portDb = System.getProperty("portdb");
		userDb = System.getProperty("userdb");
		passwordDb = System.getProperty("passworddb");
		hostSabiusDb = System.getProperty("hostsabiusdb");
		serviceSabiusDb = System.getProperty("servicesabiusdb");
		portSabiusDb = System.getProperty("portsabiusdb");
		userSabiusDb = System.getProperty("usersabiusdb");
		passwordSabiusDb = System.getProperty("passwordsabiusdb");
		
		setarValoresPadroes();
	}
	
	public static ConnectionFactory configConnMV()
	{
		return new ConnectionFactory(getHostDb(), getServiceDb(), getPortDb(), getUserDb(), getPasswordDb());
	}
	
	private static void setarValoresPadroes() 
	{
		if (uri == null 
				|| uri.equalsIgnoreCase(""))
		{
			System.setProperty("uri", "http://pjweb.unimedfortaleza.com.br/sabius-servicos-web/rest-servicos/visitaSocial/incluir");
		}
		
		if (uriAlterar == null 
				|| uriAlterar.equalsIgnoreCase(""))
		{
			System.setProperty("urialterar", "http://pjweb.unimedfortaleza.com.br/sabius-servicos-web/rest-servicos/visitaSocial/alterar");
		}
		
		if (uriListar == null 
				|| uriListar.equalsIgnoreCase(""))
		{
			System.setProperty("urilistar", "http://pjweb.unimedfortaleza.com.br/sabius-servicos-web/rest-servicos/visitaSocial/listar");
		}
		
		if (user == null 
				|| user.equalsIgnoreCase(""))
		{
			System.setProperty("user", "AUDSOCIAL");
		}
		
		if (password == null 
				|| password.equalsIgnoreCase(""))
		{
			System.setProperty("password", "au2011");
		}
		
		if (hostDb == null 
				|| hostDb.equalsIgnoreCase(""))
		{
			System.setProperty("hostdb", "racmv.unimedfortaleza.com.br");
		}
		
		if (serviceDb == null 
				|| serviceDb.equalsIgnoreCase(""))
		{
			System.setProperty("servicedb", "mv");
		}
		
		if (portDb == null 
				|| portDb.equalsIgnoreCase(""))
		{
			System.setProperty("portdb", "1528");
		}
		
		if (userDb == null 
				|| userDb.equalsIgnoreCase(""))
		{
			System.setProperty("userdb", "reader");
		}
		
		if (passwordDb == null 
				|| passwordDb.equalsIgnoreCase(""))
		{
			System.setProperty("passworddb", "reader");
		}
		
		if (hostSabiusDb == null 
				|| hostSabiusDb.equalsIgnoreCase(""))
		{
			System.setProperty("hostsabiusdb", "racsab.unimedfortaleza.com.br");
		}
		
		if (serviceSabiusDb == null 
				|| serviceSabiusDb.equalsIgnoreCase(""))
		{
			System.setProperty("servicesabiusdb", "sab");
		}
		
		if (portSabiusDb == null 
				|| portSabiusDb.equalsIgnoreCase(""))
		{
			System.setProperty("portsabiusdb", "1522");
		}
		
		if (userSabiusDb == null 
				|| userSabiusDb.equalsIgnoreCase(""))
		{
			System.setProperty("usersabiusdb", "reader");
		}
		
		if (passwordSabiusDb == null 
				|| passwordSabiusDb.equalsIgnoreCase(""))
		{
			System.setProperty("passwordsabiusdb", "reader");
		}
	}

	public static ConnectionFactory configConnSabius()
	{
		return new ConnectionFactory(getHostSabiusDb(), getServiceSabiusDb(), getPortSabiusDb(), getUserSabiusDb(), getPasswordSabiusDb());
	}

	public static String getUri() {
		return uri;
	}

	public static String getUriAlterar() {
		return uriAlterar;
	}

	public static String getUriListar() {
		return uriListar;
	}

	public static String getUser() {
		return user;
	}

	public static String getPassword() {
		return password;
	}

	public static String getHostDb() {
		return hostDb;
	}

	public static String getServiceDb() {
		return serviceDb;
	}

	public static String getPortDb() {
		return portDb;
	}

	public static String getUserDb() {
		return userDb;
	}

	public static String getPasswordDb() {
		return passwordDb;
	}

	public static String getHostSabiusDb() {
		return hostSabiusDb;
	}

	public static String getServiceSabiusDb() {
		return serviceSabiusDb;
	}

	public static String getPortSabiusDb() {
		return portSabiusDb;
	}

	public static String getUserSabiusDb() {
		return userSabiusDb;
	}

	public static String getPasswordSabiusDb() {
		return passwordSabiusDb;
	}
}