package br.com.integracao.visitasocial.request;

import java.io.Serializable;

import br.com.integracao.visitasocial.model.Objeto;

public class ApiRequest implements Serializable {

	private static final long serialVersionUID = -3389333209713569083L;

	private Objeto objeto;

	public ApiRequest() {
		
	}
	
	public ApiRequest(Objeto objeto) {
		this.objeto = objeto;
	}

	public Objeto getObjeto() {
		return objeto;
	}

	public void setObjeto(Objeto objeto) {
		this.objeto = objeto;
	}
}