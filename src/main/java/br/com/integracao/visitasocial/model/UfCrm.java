package br.com.integracao.visitasocial.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UfCrm implements Serializable {

	private static final long serialVersionUID = -3389333209713569083L;

	private String codUf;

	public UfCrm()
	{
		
	}
	
	public UfCrm(String codUf) 
	{
		super();
		this.codUf = codUf;
	}

	public String getCodUf() {
		return codUf;
	}

	public void setCodUf(String codUf) {
		this.codUf = codUf;
	}
}