package br.com.integracao.visitasocial.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class VisitaSocialRetornoListar implements Serializable {

	private static final long serialVersionUID = -3389333209713569083L;

	private Integer idCadastroVisitaSocial;

	private Integer diasInternacao;

	private VisitaSocial vsVisitaSocial;

	public Integer getIdCadastroVisitaSocial() {
		return idCadastroVisitaSocial;
	}

	public void setIdCadastroVisitaSocial(Integer idCadastroVisitaSocial) {
		this.idCadastroVisitaSocial = idCadastroVisitaSocial;
	}

	public Integer getDiasInternacao() {
		return diasInternacao;
	}

	public void setDiasInternacao(Integer diasInternacao) {
		this.diasInternacao = diasInternacao;
	}
	
	public VisitaSocial getVsVisitaSocial() {
		return vsVisitaSocial;
	}

	public void setVsVisitaSocial(VisitaSocial vsVisitaSocial) {
		this.vsVisitaSocial = vsVisitaSocial;
	}

	@Override
	public String toString() {
		return "VisitaSocialRetornoListar [idCadastroVisitaSocial=" + idCadastroVisitaSocial + ", diasInternacao="
				+ diasInternacao + ", vsVisitaSocial=" + vsVisitaSocial + "]";
	}
}