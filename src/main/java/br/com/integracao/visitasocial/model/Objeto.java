package br.com.integracao.visitasocial.model;

import java.io.Serializable;

import br.com.integracao.visitasocial.request.VisitaSocialRequest;

public class Objeto implements Serializable {

	private static final long serialVersionUID = -3389333209713569083L;

	private String url;

	private VisitaSocialRequest json;
	
	private String metodo;
	
	private String authorization;
	
	public Objeto(String url, VisitaSocialRequest json, String metodo, String authorization) {
		super();
		this.url = url;
		this.json = json;
		this.metodo = metodo;
		this.authorization = authorization;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public VisitaSocialRequest getJson() {
		return json;
	}

	public void setJson(VisitaSocialRequest json) {
		this.json = json;
	}

	public String getMetodo() {
		return metodo;
	}

	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}

	public String getAuthorization() {
		return authorization;
	}

	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}
}